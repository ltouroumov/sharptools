using System;

namespace SharpTools.Geometry
{
	public class Point2D
	{
		public double x { get; private set; }
		public double y { get; private set; }

		public static Point2D At(double x, double y)
		{
			return new Point2D(x, y);
		}

		public Point2D(double x, double y)
		{
			this.x = x;
			this.y = y;
		}

		public Point2D RelativeTo(Point2D origin)
		{
			return Point2D.At(this.x - origin.x, this.y - origin.y);
		}

		public Point2D Map(Func<double, double> xMap, Func<double, double> yMap)
		{
			return Point2D.At(xMap(this.x), yMap(this.y));
		}

		public override string ToString()
		{
			return string.Format("[Point2D: x={0:0.###}, y={1:0.###}]", x, y);
		}
	}
}

