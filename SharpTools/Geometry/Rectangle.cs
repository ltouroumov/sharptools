using System;
using System.Linq;

namespace SharpTools.Geometry
{
	public class Rectangle
	{
		public Point2D Origin { get; private set; }
		public Size2D Size { get; private set; }

		public Rectangle(Point2D origin, Size2D size)
		{
			this.Origin = origin;
			this.Size = size;
		}

		public Rectangle(double x, double y, double width, double height) : this(new Point2D(x, y), new Size2D(width, height))
		{

		}

		private Point2D[,] _vertices = null;
		public Point2D[,] Vertices() {
			if (_vertices == null) {
				var x0 = Origin.x;
				var x1 = x0 + Size.Width;
				var y0 = Origin.y;
				var y1 = y0 + Size.Height;

				_vertices = new Point2D[,] {
					{ new Point2D(x0, y0), new Point2D(x0, y1) },
					{ new Point2D(x1, y0), new Point2D(x1, y1) }
				};
			}

			return _vertices;
		}

		public bool Contains(Point2D point)
		{
			var x0 = Origin.x;
			var x1 = x0 + Size.Width;
			var y0 = Origin.y;
			var y1 = y0 + Size.Height;

			return (x0 <= point.x && point.x <= x1) && (y0 <= point.y && point.y <= y1);
		}

		public bool Contains(Rectangle other)
		{
			// If we contains all the vertices then we contain the rectangle
			return other.Vertices().Cast<Point2D>().Any(this.Contains);
		}

		public bool Intersects(Rectangle other)
		{
			// If we contain any of the vertices then we intersect with the rectangle
			return other.Vertices().Cast<Point2D>().Any(this.Contains);
		}

		public override string ToString()
		{
			return string.Format("[Rectangle: Origin={0}, Size={1}]", Origin, Size);
		}
	}
}

